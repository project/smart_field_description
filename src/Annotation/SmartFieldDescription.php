<?php

namespace Drupal\smart_field_description\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Smart field description item annotation object.
 *
 * @see \Drupal\smart_field_description\Plugin\SmartFieldDescriptionManager
 * @see plugin_api
 *
 * @Annotation
 */
class SmartFieldDescription extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
