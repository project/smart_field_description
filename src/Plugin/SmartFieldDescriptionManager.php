<?php

namespace Drupal\smart_field_description\Plugin;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides the Smart field description plugin manager.
 */
class SmartFieldDescriptionManager extends DefaultPluginManager {
  use StringTranslationTrait;

  /**
   * Constructs a new SmartFieldDescriptionManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/SmartFieldDescription', $namespaces, $module_handler, 'Drupal\smart_field_description\Plugin\SmartFieldDescriptionInterface', 'Drupal\smart_field_description\Annotation\SmartFieldDescription');

    $this->alterInfo('smart_field_description_smart_field_description_info');
    $this->setCacheBackend($cache_backend, 'smart_field_description_smart_field_description_plugins');
  }

  /**
   * Retrieves the renderable array from the plugin to be used as description.
   *
   * Each SmartFieldDescription plugin defines how to render the field
   * description using the render method. This method wraps this logic to be
   * used outside the manager to facilitate its usage.
   *
   * @param array $context
   *   The context array. This is passed from hook_field_widget_form_alter().
   *
   * @return array
   *   The field description renderable array defined by the configured plugin.
   */
  public function getDescription(array $context) : array {
    $render = [];

    $handler = $this->getHandler($context);
    if ($handler instanceof SmartFieldDescriptionInterface) {
      $render = $handler->render();
    }

    return $render;
  }

  /**
   * Build the list of available Smart description handlers.
   *
   * @return array
   *   The array of handlers options.
   */
  public function getHandlersOptions() : array {
    $handlers = [];

    foreach ($this->getDefinitions() as $definition) {
      $handlers[$definition['id']] = $definition['label'];
    }

    return $handlers;
  }

  /**
   * Retrieves the smart field description plugin definition from the widget.
   *
   * @param \Drupal\Core\Field\WidgetInterface $widget
   *   The WidgetInterface from which we get what handler definition should be
   *   retrieved.
   *
   * @return array|null
   *   The plugin definition array defined by the Annotation.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getHandlerDefinition(WidgetInterface $widget) {
    $settings = $this->getSettings($widget);
    return $this->getDefinition($settings['smart_description_handler']);
  }

  /**
   * Retrieves the Smart Field Description handler plugin instance.
   *
   * @param array $context
   *   The context array.
   *
   * @return \Drupal\smart_field_description\Plugin\SmartFieldDescriptionInterface|null
   *   The SmartFieldDescriptionInterface plugin instance.
   */
  protected function getHandler(array $context) {
    $handler = NULL;

    // Get the configured SmartDescription if any:
    /** @var \Drupal\Core\Field\WidgetInterface $widget */
    $widget = $context['widget'];

    if ($this->hasSmartFieldDescriptionEnabled($widget)) {
      $settings = $this->getSettings($widget);

      try {
        $configuration = [
          'widget' => $widget,
          'field_definition' => $context['items']->getFielddefinition(),
        ];

        /** @var \Drupal\smart_field_description\Plugin\SmartFieldDescriptionInterface $handler */
        $handler = $this->createInstance($settings['smart_description_handler'], $configuration);
      }
      catch (PluginException $exception) {
        // TODO Manage the exception here.
      }
    }

    return $handler;
  }

  /**
   * Retrieves the smart_field_description settings from the widget.
   *
   * @param \Drupal\Core\Field\WidgetInterface $widget
   *   The widget configuration.
   *
   * @return array
   *   The smart_field_description settings array.
   */
  public function getSettings(WidgetInterface $widget) : array {
    return $widget->getThirdPartySettings('smart_field_description');
  }

  /**
   * Checks whether smart_field_description is set for the give widget or not.
   *
   * It checks if there is any handler defined on the widget instance.
   *
   * @param \Drupal\Core\Field\WidgetInterface $widget
   *   The widget configuration.
   *
   * @return bool
   *   TRUE if the given widget has defined a handler to be used.
   *   FALSE otherwise.
   */
  public function hasSmartFieldDescriptionEnabled(WidgetInterface $widget) : bool {
    $settings = $this->getSettings($widget);
    return !empty($settings['smart_description_handler']);
  }

}
