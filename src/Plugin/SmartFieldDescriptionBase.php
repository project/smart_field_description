<?php

namespace Drupal\smart_field_description\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Smart field description plugins.
 */
abstract class SmartFieldDescriptionBase extends PluginBase implements SmartFieldDescriptionInterface {

  /**
   * The widget which will render the description.
   *
   * @var \Drupal\Core\Field\WidgetInterface
   */
  protected $widget;

  /**
   * The field definition of the field which we are rendering the description.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $fieldDefinition;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->widget = $configuration['widget'];
    $this->fieldDefinition = $configuration['field_definition'];
  }

  /**
   * Retrieves the raw description string from the field definition.
   *
   * @return string|null
   *   The configured field description from the field instance definition.
   */
  protected function getRawDescription() {
    return $this->fieldDefinition->getDescription();
  }

}
