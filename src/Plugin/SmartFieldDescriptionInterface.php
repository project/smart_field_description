<?php

namespace Drupal\smart_field_description\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Smart field description plugins.
 */
interface SmartFieldDescriptionInterface extends PluginInspectionInterface {

  /**
   * Builds the renderable array to be used to show the field description.
   *
   * @return array
   *   The renderable array to print the field description.
   */
  public function render() : array;

}
